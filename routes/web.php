<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['middleware'=>'auth'],function ($router) {
$router->get('/orders','OrdersController@getOrder');
$router->get('/me','AuthController@me');
$router->get('/orders/{id}','OrdersController@detailOrder');
$router->get('/logout','AuthController@logout');
});
$router->group(['middleware'=>'throttle:1,0.20'],function ($router) {
    $router->post('/orders','OrdersController@postOrder');
    });
    


$router->post('/register','AuthController@register');
$router->post('/login','AuthController@login');
$router->get('/myinq/{token}','OrdersController@getOrdersbyToken');

