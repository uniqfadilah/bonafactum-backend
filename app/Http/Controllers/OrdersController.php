<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\References;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTFactory;
use  Tymon\JWTAuth\Facades\JWTAuth;
use  Tymon\JWTAuth\Facades\JWT;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class OrdersController extends Controller
{

    public function getOrder(){
        $data = Orders::with('images')->orderBy('created_at', 'desc')->paginate(5);
        return response()->json($data,200);
    }
    public function getOrdersbyToken($token){
        try {
            $decrypted = Crypt::decrypt($token);
            $data = Orders::where('id', $decrypted)->with('images')->first();
             return response()->json($data,200);
        } catch (DecryptException $e) {
            return response(404);
        }

    }
    public function detailOrder($id){
        $data = Orders::where('id', $id)->with('images')->first();
        return response()->json($data,200);
    }
    public function postOrder(Request $request){
        $validator = $this->validate($request, [
            'contact_person' => 'required',
            'email' => 'required',
            'country' => 'required',
            'zip_code' => 'required',
            'minimum_order' => 'required | numeric',
            'acceptable_lead_time' => 'required | numeric',
            'type'=> 'required',
            'material' => 'required',
        ]);       
        $data= Orders::create([
            'contact_person' => $request->contact_person,
            'email' => $request->email,
            'country' => $request->country,
            'zip_code' => $request->zip_code,
            'minimum_order' => $request->minimum_order,
            'acceptable_lead_time' => $request->acceptable_lead_time,
            'type'=> $request->type,
            'material' => $request->material,
            'description' =>$request->description
        ]);
  
        if($request->hasFile('preference')){
            $items = array();
           
            foreach($request->file('preference') as $file){
                $image_name = md5(rand(1000,10000));
                $ext = strtolower($file->getClientOriginalExtension());
                $image_full_name = $image_name.'.'.$ext;
                $uploade_path = 'images/';
                $image_url = $uploade_path.$image_full_name;
                $items[] = Storage::put($uploade_path, $file);
               
            }

            foreach($items as $i){
                References::create([
                    'order_id' =>$data->id,
                    'url' => $i,
                ]);
            }
        }
        $token = Crypt::encrypt($data->id);
        $test = [
            'name' => $data->contact_person,
            'email' => $data->email,
            'token' =>$token
        ]; 

        
        Mail::send([], [], function($message) use ($data ,$token) {
        $message->to($data->email)->subject('Requiment Bonafactum');
        $message->from('killmynamezz@gmail.com','Bonafactum');
        $message->setBody('<p><strong>Hi, '.$data->contact_person.'!</strong></p>
        <p>Thank you for sending us<a href="https://bonafactum.herokuapp.com/order/'.$token.'">&nbsp;Your Inquiry</a></p>
        <p>If you have more question or Come accros any other issue, let us know, we will happy to helo</p>
        <p>Have a great day,</p>
        <p><strong>Unique7ewelry</strong></p>', 'text/html');
        });
        return response($token);
    }
}
