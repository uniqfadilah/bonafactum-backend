<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $guarded = [];  
    protected $table = 'orders'; 
    public function images(){
        return $this->hasMany('App\Models\References','order_id','id');
    }
  
}

