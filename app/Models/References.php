<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class References extends Model
{
    protected $guarded = [];  
    protected $table = 'references'; 
    protected $appends = ['full_url'];

    public function getFullUrlAttribute(){
        return env('BASE_URL').'/storage/'.$this->url;
    }
}